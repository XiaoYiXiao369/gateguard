package io.github.igordonxiao.gateguard.exception;

/**
 * 用户异常
 */
public interface UserExceptions {
    RuntimeException USER_NOT_FOUND = new RuntimeException("未找到用户");
    RuntimeException DUPLICATE_USER_CODE = new RuntimeException("已存在相同登录名的用户，换一个试试");
    RuntimeException DUPLICATE_USER_NAME = new RuntimeException("已存在相同用户名的用户，换一个试试");
    RuntimeException CAN_NOT_DELETE_SELF = new RuntimeException("不能删除自己");
    RuntimeException USER_NOT_LOGIN = new RuntimeException("用户未登录");
    RuntimeException BAD_USER = new RuntimeException("用户参数不正确");
}
