package io.github.igordonxiao.gateguard.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.github.igordonxiao.gateguard.domain.User;

/**
 * 用户数据库操作类
 */
public interface UserMapper extends BaseMapper<User> {
}
