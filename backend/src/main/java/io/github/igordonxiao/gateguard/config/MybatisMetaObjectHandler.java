package io.github.igordonxiao.gateguard.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import io.github.igordonxiao.gateguard.util.SecurityUtils;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Mybatis plus自动填充处理器
 */
@Component
public class MybatisMetaObjectHandler implements MetaObjectHandler {

    /**
     * 新增时填充设置
     *
     * @param metaObject 元对象
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        Date curTime = new Date();
        // 设置创建时间
        this.setFieldValByName("createdTime", curTime, metaObject);
        // 设置创建人
        String userId = SecurityUtils.getLoginUserId();
        // 依赖当前回话信息
        this.setFieldValByName("createdUser", userId, metaObject);
        // 设置更新时间
        this.setFieldValByName("updatedTime", curTime, metaObject);
        // 设置更新人
        this.setFieldValByName("updatedUser", userId, metaObject);
    }

    /**
     * 更新时填充设置
     *
     * @param metaObject 元对象
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        // 设置更新时间
        this.setFieldValByName("updatedTime", new Date(), metaObject);
        // 设置更新人
        this.setFieldValByName("updatedUser", SecurityUtils.getLoginUserId(), metaObject);
    }


}

