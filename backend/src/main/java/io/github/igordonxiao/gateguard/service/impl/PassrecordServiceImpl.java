package io.github.igordonxiao.gateguard.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.github.igordonxiao.gateguard.domain.Passrecord;
import io.github.igordonxiao.gateguard.domain.Resident;
import io.github.igordonxiao.gateguard.exception.ResidentExceptions;
import io.github.igordonxiao.gateguard.mapper.PassrecordMapper;
import io.github.igordonxiao.gateguard.service.PassrecordService;
import io.github.igordonxiao.gateguard.service.ResidentService;
import io.github.igordonxiao.gateguard.vo.PassrecordVo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * 出行记录服务类
 */
@AllArgsConstructor
@Slf4j
@Service
public class PassrecordServiceImpl extends ServiceImpl<PassrecordMapper, Passrecord> implements PassrecordService {

    @Autowired
    ResidentService residentService;

    @Override
    public Passrecord createPassrecord(PassrecordVo passrecordVo) {
        String residentId = passrecordVo.getResidentId();
        Resident resident = residentService.getById(residentId);
        if (resident == null) {
            throw ResidentExceptions.RESIDENT_NOT_FOUND;
        }
        Passrecord passrecord = new Passrecord();
        passrecord.setPhase(resident.getPhase());
        passrecord.setBuilding(resident.getBuilding());
        passrecord.setUnit(resident.getUnit());
        passrecord.setFloor(resident.getFloor());
        passrecord.setNumber(resident.getNumber());
        passrecord.setName(resident.getName());

        passrecord.setDirect(passrecordVo.getDirect());
        passrecord.setTemperature(passrecordVo.getTemperature());
        passrecord.setRemark(passrecordVo.getRemark());
        passrecord.setResidentId(passrecordVo.getResidentId());

        passrecord.setId(UUID.randomUUID().toString());
        this.save(passrecord);
        return passrecord;
    }
}
