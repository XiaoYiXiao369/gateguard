package io.github.igordonxiao.gateguard.vo;

import io.github.igordonxiao.gateguard.domain.BaseDomain;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 用户展示类
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class UserVo extends BaseDomain {
    private String code;
    private String name;
    private String phone;
}
