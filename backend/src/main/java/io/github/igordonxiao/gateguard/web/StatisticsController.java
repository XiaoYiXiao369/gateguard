package io.github.igordonxiao.gateguard.web;

import io.github.igordonxiao.gateguard.service.PassrecordService;
import io.github.igordonxiao.gateguard.service.ResidentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/statistics")
public class StatisticsController {

    @Autowired
    ResidentService residentService;

    @Autowired
    PassrecordService passrecordService;

    @GetMapping
    ResponseEntity<Map<String, Object>> count() {
        Map<String, Object> countMap = new HashMap<>();
        countMap.put("residentCount", residentService.count());
        countMap.put("passrecordCount", passrecordService.count());
        return ResponseEntity.ok(countMap);
    }
}
