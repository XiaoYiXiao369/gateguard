package io.github.igordonxiao.gateguard.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.github.igordonxiao.gateguard.domain.Resident;
import io.github.igordonxiao.gateguard.domain.User;

/**
 * 居民数据库操作类
 */
public interface ResidentMapper extends BaseMapper<Resident> {
}
