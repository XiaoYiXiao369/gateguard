package io.github.igordonxiao.gateguard.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.github.igordonxiao.gateguard.domain.Passrecord;
import io.github.igordonxiao.gateguard.service.PassrecordService;
import io.github.igordonxiao.gateguard.vo.PassrecordVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static io.github.igordonxiao.gateguard.config.Const.DEFAULT_PAGE_NO;
import static io.github.igordonxiao.gateguard.config.Const.DEFAULT_PAGE_SIZE;

/**
 * 出行记录接口
 */
@RestController
@RequestMapping("/api/passrecords")
public class PassrecordController {

    @Autowired
    PassrecordService passrecordService;

    @GetMapping
    ResponseEntity<IPage<Passrecord>> list(@RequestParam(required = false) Integer pageNo,
                                           @RequestParam(required = false) Integer pageSize,
                                           @RequestParam(required = false) Integer phase,
                                           @RequestParam(required = false) Integer building,
                                           @RequestParam(required = false) Integer unit,
                                           @RequestParam(required = false) Integer floor,
                                           @RequestParam(required = false) Integer number,
                                           @RequestParam(required = false) String name
    ) {
        if (pageNo == null) {
            pageNo = DEFAULT_PAGE_NO;
        }
        if (pageSize == null) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        IPage<Passrecord> page = passrecordService
                .lambdaQuery()
                .eq(phase != null && phase > 0, Passrecord::getPhase, phase)
                .eq(building != null && building > 0, Passrecord::getBuilding, building)
                .eq(unit != null && unit > 0, Passrecord::getUnit, unit)
                .eq(floor != null && floor > 0, Passrecord::getFloor, floor)
                .eq(number != null && number > 0, Passrecord::getNumber, number)
                .like(!StringUtils.isEmpty(name), Passrecord::getName, name)
                .orderByDesc(Passrecord::getCreatedTime)
                .page(new Page<>(pageNo, pageSize));
        return ResponseEntity.ok(page);
    }

    @PostMapping
    ResponseEntity<Passrecord> createPassrecord(@RequestBody @NotNull PassrecordVo passrecordVo) {
        return ResponseEntity.ok(passrecordService.createPassrecord(passrecordVo));
    }

    @GetMapping("/todayHistory/{residentId}")
    ResponseEntity<List<Passrecord>> todayPassHistory(@PathVariable String residentId) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date todayStart = calendar.getTime();
        calendar.set(Calendar.HOUR_OF_DAY, 24);
        Date todayEnd = calendar.getTime();
        return ResponseEntity.ok(passrecordService.lambdaQuery().orderByDesc(Passrecord::getCreatedTime).eq(Passrecord::getResidentId, residentId).between(Passrecord::getCreatedTime, todayStart, todayEnd).list());
    }

}
