package io.github.igordonxiao.gateguard.config;

/**
 * 常量类
 */
public interface Const {
    Integer DEFAULT_PAGE_NO = 1;

    Integer DEFAULT_PAGE_SIZE = 10;
}
