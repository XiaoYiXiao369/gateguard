package io.github.igordonxiao.gateguard.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.igordonxiao.gateguard.vo.UserVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 用户（即管理员）
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class User extends BaseDomain implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 登录名
     */
    @NotBlank(message = "登录名不能为空")
    @Length(min = 5, max = 32, message = "登录名长度为5到32位")
    private String code;
    /**
     * 名称
     */
    @NotBlank(message = "名称不能为空")
    @Length(min = 1, max = 32, message = "项目名称长度为1到32位")
    private String name;
    /**
     * 密码
     */
    @NotBlank(message = "密码不能为空")
    @Length(min = 6, max = 32, message = "项目名称长度为6到32位")
    private String password;

}
