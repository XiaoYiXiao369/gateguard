package io.github.igordonxiao.gateguard.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

/**
 * 错误响应对象
 */
@Data
@AllArgsConstructor
public class ResponseErrorEntity {
    /**
     * http 响应状态
     */
    private String status;
    /**
     * http 请求路径
     */
    private String path;

    /**
     * http 响应消息
     */
    private String message;

    /**
     * http 响应时间
     */
    private Date timestamp;
}
