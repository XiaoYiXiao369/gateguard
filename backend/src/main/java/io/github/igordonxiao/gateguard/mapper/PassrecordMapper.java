package io.github.igordonxiao.gateguard.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.github.igordonxiao.gateguard.domain.Passrecord;

/**
 * 通行记录据库操作类
 */
public interface PassrecordMapper extends BaseMapper<Passrecord> {
}
