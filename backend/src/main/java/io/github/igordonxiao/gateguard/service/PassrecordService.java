package io.github.igordonxiao.gateguard.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.github.igordonxiao.gateguard.domain.Passrecord;
import io.github.igordonxiao.gateguard.vo.PassrecordVo;

/**
 * 通行记录服务类
 */
public interface PassrecordService extends IService<Passrecord> {

    Passrecord createPassrecord(PassrecordVo passrecordVo);
}