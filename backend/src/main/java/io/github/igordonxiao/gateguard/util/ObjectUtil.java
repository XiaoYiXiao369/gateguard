package io.github.igordonxiao.gateguard.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 对象处理工具类
 */
public class ObjectUtil {
    private final static ObjectMapper mapper;

    static {
        mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    private ObjectUtil() {
    }

    public static <T, R> R domainToVo(T t, Class<? extends R> clazz) {
        return mapper.convertValue(t, clazz);
    }

}
