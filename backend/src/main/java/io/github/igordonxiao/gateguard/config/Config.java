package io.github.igordonxiao.gateguard.config;

/**
 * 配置类
 */
public interface Config {

    /**
     * 限制客户端的访问范围
     */
    String SCOPES = "openApi";

    /**
     * 资源ID
     */
    String RESOURCE_ID = "api";

    /**
     * UTF-8编码
     */
    String APPLICATION_UTF8 = "application/json;charset=UTF-8";

}
