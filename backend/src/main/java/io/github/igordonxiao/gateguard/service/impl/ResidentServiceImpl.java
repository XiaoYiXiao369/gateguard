package io.github.igordonxiao.gateguard.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.github.igordonxiao.gateguard.domain.Resident;
import io.github.igordonxiao.gateguard.mapper.ResidentMapper;
import io.github.igordonxiao.gateguard.service.ResidentService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 居民服务类
 */
@AllArgsConstructor
@Slf4j
@Service
public class ResidentServiceImpl extends ServiceImpl<ResidentMapper, Resident> implements ResidentService {


}
