package io.github.igordonxiao.gateguard.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 居民
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class Resident extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 2L;

    /**
     * 小区期数
     */
    @NotNull(message = "小区期数不能为空")
    @Length(min = 1, max = 99, message = "小区期数最大支持32期")
    private Integer phase;

    /**
     * 栋数
     */
    @NotNull(message = "栋数不能为空")
    @Length(min = 1, max = 300, message = "小区栋数最大支持300栋")
    private Integer building;

    /**
     * 单元
     */
    @NotNull(message = "单元不能为空")
    @Length(min = 1, max = 100, message = "小区单元数最大支持50个")
    private Integer unit;

    /**
     * 楼层
     */
    @NotNull(message = "楼层不能为空")
    @Length(min = 1, max = 100, message = "小区楼层数最大支持100层")
    private Integer floor;

    /**
     * 房号
     */
    @NotNull(message = "房号不能为空")
    @Length(min = 1, max = 100, message = "小区房号数最大支持50")
    private Integer number;

    /**
     * 居民姓名
     */
    @NotBlank(message = "居民姓名不能为空")
    @Length(min = 2, max = 30, message = "姓名最大支持30个字符")
    private String name;

    /**
     * 居名身份证号
     */
    @NotNull(message = "居名身份证号不能为空")
    @Length(min = 18, max = 18, message = "居名身份证号需要18位")
    private String idCard;

    /**
     * 手机号
     */
    private String phone;
}
