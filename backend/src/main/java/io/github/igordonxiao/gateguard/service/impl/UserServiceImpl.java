package io.github.igordonxiao.gateguard.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.github.igordonxiao.gateguard.domain.User;
import io.github.igordonxiao.gateguard.mapper.UserMapper;
import io.github.igordonxiao.gateguard.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户服务类
 */
@AllArgsConstructor
@Slf4j
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {


    @Override
    public User findUserByCode(String code) {
        List<User> users = this.findUsersByCode(code);
        if (users.size() > 0) {
            return users.get(0);
        }
        return null;
    }

    @Override
    public List<User> findUsersByCode(String code) {
        LambdaQueryWrapper<User> query = new QueryWrapper<User>().lambda().eq(User::getCode, code);
        return this.baseMapper.selectList(query);
    }

    @Override
    public User findUserByName(String name) {
        List<User> users = this.findUsersByName(name);
        if (users.size() > 0) {
            return users.get(0);
        }
        return null;
    }

    @Override
    public List<User> findUsersByName(String name) {
        LambdaQueryWrapper<User> query = new QueryWrapper<User>().lambda().eq(User::getName, name);
        return this.baseMapper.selectList(query);
    }


}
