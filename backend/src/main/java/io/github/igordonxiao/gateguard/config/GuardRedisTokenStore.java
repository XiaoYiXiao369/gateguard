package io.github.igordonxiao.gateguard.config;

import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
import org.springframework.stereotype.Component;

/**
 * Redis存储Token
 */
@Component
public class GuardRedisTokenStore extends RedisTokenStore {
    public GuardRedisTokenStore(RedisConnectionFactory connectionFactory) {
        super(connectionFactory);
    }
}
