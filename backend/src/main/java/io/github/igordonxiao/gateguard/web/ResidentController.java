package io.github.igordonxiao.gateguard.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.github.igordonxiao.gateguard.domain.Resident;
import io.github.igordonxiao.gateguard.service.ResidentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static io.github.igordonxiao.gateguard.config.Const.DEFAULT_PAGE_NO;
import static io.github.igordonxiao.gateguard.config.Const.DEFAULT_PAGE_SIZE;

/**
 * 居民接口
 */
@RestController
@RequestMapping("/api/residents")
public class ResidentController {

    @Autowired
    ResidentService residentService;

    @GetMapping
    ResponseEntity<IPage<Resident>> page(@RequestParam(required = false) Integer pageNo,
                                         @RequestParam(required = false) Integer pageSize,
                                         @RequestParam(required = false) Integer phase,
                                         @RequestParam(required = false) Integer building,
                                         @RequestParam(required = false) Integer unit,
                                         @RequestParam(required = false) Integer floor,
                                         @RequestParam(required = false) Integer number,
                                         @RequestParam(required = false) String name
    ) {
        if (pageNo == null) {
            pageNo = DEFAULT_PAGE_NO;
        }
        if (pageSize == null) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        IPage<Resident> page = residentService
                .lambdaQuery()
                .eq(phase != null && phase > 0, Resident::getPhase, phase)
                .eq(building != null && building > 0, Resident::getBuilding, building)
                .eq(unit != null && unit > 0, Resident::getUnit, unit)
                .eq(floor != null && floor > 0, Resident::getFloor, floor)
                .eq(number != null && number > 0, Resident::getNumber, number)
                .like(!StringUtils.isEmpty(name), Resident::getName, name)
                .orderByDesc(Resident::getCreatedTime)
                .page(new Page<>(pageNo, pageSize));
        return ResponseEntity.ok(page);
    }

    @PostMapping
    ResponseEntity<Resident> saveResident(@RequestBody Resident resident) {
        String id = resident.getId();
        if (StringUtils.isEmpty(id)) {
            resident.setId(UUID.randomUUID().toString());
            residentService.save(resident);
            return ResponseEntity.ok(resident);
        }
        Resident dbResident = residentService.getById(id);
        dbResident.setPhase(resident.getPhase());
        dbResident.setBuilding(resident.getBuilding());
        dbResident.setFloor(resident.getFloor());
        dbResident.setNumber(resident.getNumber());
        dbResident.setName(resident.getName());
        dbResident.setIdCard(resident.getIdCard());
        dbResident.setPhone(resident.getPhone());
        residentService.updateById(dbResident);
        return ResponseEntity.ok(dbResident);
    }

    @GetMapping("/{id}")
    ResponseEntity<Resident> findById(@PathVariable String id) {
        return ResponseEntity.ok(residentService.getById(id));
    }

    @DeleteMapping("/{id}")
    ResponseEntity<Boolean> deleteResidentById(@PathVariable String id) {
        return ResponseEntity.ok(residentService.removeById(id));
    }

}
