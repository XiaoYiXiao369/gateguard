package io.github.igordonxiao.gateguard.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.github.igordonxiao.gateguard.domain.Resident;
import io.github.igordonxiao.gateguard.domain.User;

import java.util.List;

/**
 * 用户服务类
 */
public interface UserService extends IService<User> {
    User findUserByCode(String code);

    List<User> findUsersByCode(String code);

    User findUserByName(String name);

    List<User> findUsersByName(String name);
}