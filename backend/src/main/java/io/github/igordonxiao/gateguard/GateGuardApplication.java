package io.github.igordonxiao.gateguard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 */
@SpringBootApplication
public class GateGuardApplication {

    public static void main(String[] args) {
        SpringApplication.run(GateGuardApplication.class, args);
    }

}
