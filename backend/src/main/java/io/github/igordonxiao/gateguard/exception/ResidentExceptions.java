package io.github.igordonxiao.gateguard.exception;

/**
 * 居民异常
 */
public interface ResidentExceptions {
    RuntimeException RESIDENT_NOT_FOUND = new RuntimeException("未找到居民");
}
