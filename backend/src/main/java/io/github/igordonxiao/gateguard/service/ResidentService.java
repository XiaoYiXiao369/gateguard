package io.github.igordonxiao.gateguard.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.github.igordonxiao.gateguard.domain.Resident;

/**
 * 居民服务类
 */
public interface ResidentService extends IService<Resident> {

}