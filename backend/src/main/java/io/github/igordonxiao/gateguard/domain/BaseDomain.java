package io.github.igordonxiao.gateguard.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

/**
 * 基础Domain
 */
@Data
public class BaseDomain {
    @TableId(value = "id")
    private String id;

    @TableField(value = "created_user", updateStrategy = FieldStrategy.NOT_EMPTY, fill = FieldFill.INSERT)
    private String createdUser;

    /**
     * 在新增的时候自动填充
     */
    @TableField(value = "created_time", updateStrategy = FieldStrategy.NOT_NULL, fill = FieldFill.INSERT)
    private Date createdTime;


    @TableField(value = "updated_user", fill = FieldFill.INSERT_UPDATE)
    private String updatedUser;

    /**
     * 新增或者修改的时候自动填充.
     */
    @TableField(value = "updated_time", fill = FieldFill.INSERT_UPDATE)
    private Date updatedTime;
}
