package io.github.igordonxiao.gateguard.vo;

import lombok.Data;

/**
 * 通行记录数据传输对象，接收创建通行记录
 */
@Data
public class PassrecordVo {
    private String residentId;
    private Integer direct;
    private Integer temperature;
    private String remark;
}
