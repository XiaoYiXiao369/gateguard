// eslint-disable-next-line
import { UserLayout, BasicLayout, RouteView, BlankLayout, PageView } from '@/layouts'
import { bxAnaalyse } from '@/core/icons'

export const asyncRouterMap = [

  {
    path: '/',
    name: 'index',
    component: BasicLayout,
    meta: { title: '首页' },
    redirect: '/main/workplace',
    children: [
      // dashboard
      {
        path: '/main',
        name: 'main',
        redirect: '/main/workplace',
        component: RouteView,
        meta: { title: '仪表盘', keepAlive: true, icon: bxAnaalyse },
        children: [
          {
            path: '/main/workplace',
            name: 'Workplace',
            component: () => import('@/views/main/Workplace'),
            meta: { title: '工作台', keepAlive: true }
          }
        ]
      },
      // 通行记录
      {
        path: '/gatepass',
        name: 'gatepass',
        redirect: '/gatepass',
        component: RouteView,
        meta: { title: '门岗通行', keepAlive: true, icon: 'safety-certificate' },
        children: [
          {
            path: '/passrecord/list',
            name: 'PassrecordList',
            component: () => import('@/views/passrecord/Passrecord'),
            meta: { title: '通行记录', keepAlive: true }
          }
        ]
      },
      // 居民信息
      {
        path: '/resident',
        name: 'resident',
        redirect: '/resident',
        component: RouteView,
        meta: { title: '居民信息', keepAlive: true, icon: 'idcard' },
        children: [
          {
            path: '/resident/list',
            name: 'ResidentList',
            component: () => import('@/views/resident/Resident'),
            meta: { title: '居民列表', keepAlive: true }
          }
        ]
      },
      // 系统设置
      {
        path: '/sys',
        name: 'sys',
        redirect: '/sys',
        component: RouteView,
        meta: { title: '系统设置', keepAlive: true, icon: 'setting' },
        children: [
          {
            path: '/sys/user/list',
            name: 'SysUserList',
            component: () => import('@/views/sys/user/User'),
            meta: { title: '用户列表', keepAlive: true }
          }
        ]
      }
    ]
  },
  {
    path: '*', redirect: '/404', hidden: true
  }
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [
  {
    path: '/user',
    component: UserLayout,
    redirect: '/user/login',
    hidden: true,
    children: [
      {
        path: 'login',
        name: 'login',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/Login')
      },
      {
        path: 'register',
        name: 'register',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/Register')
      },
      {
        path: 'register-result',
        name: 'registerResult',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/RegisterResult')
      },
      {
        path: 'recover',
        name: 'recover',
        component: undefined
      }
    ]
  },

  {
    path: '/test',
    component: BlankLayout,
    redirect: '/test/home',
    children: [
      {
        path: 'home',
        name: 'TestHome',
        component: () => import('@/views/Home')
      }
    ]
  },

  {
    path: '/404',
    component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404')
  }

]
