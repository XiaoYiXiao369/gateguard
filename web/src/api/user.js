import { axios, toQueryString } from '@/utils/request'

export function getUserList (parameter) {
  return axios({
    url: '/users' + toQueryString(parameter),
    method: 'get',
    data: parameter
  })
}

export function editUser (parameter) {
  return axios({
    url: '/users',
    method: 'post',
    data: parameter
  })
}

export function deleteUser (id) {
  return axios({
    url: '/users/' + id,
    method: 'delete',
    data: {}
  })
}
