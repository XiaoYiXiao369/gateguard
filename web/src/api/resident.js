import { axios, toQueryString } from '@/utils/request'

export function getResidentList (parameter) {
  return axios({
    url: '/residents' + toQueryString(parameter),
    method: 'get',
    data: parameter
  })
}

export function editResident (parameter) {
  return axios({
    url: '/residents',
    method: 'post',
    data: parameter
  })
}

export function deleteResident (id) {
  return axios({
    url: '/residents/' + id,
    method: 'delete',
    data: {}
  })
}
