import { axios } from '@/utils/request'

export function getCount (parameter) {
  return axios({
    url: '/statistics',
    method: 'get',
    data: parameter
  })
}
