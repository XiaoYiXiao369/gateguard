import { axios, toQueryString } from '@/utils/request'

export function getPassrecordList (parameter) {
  return axios({
    url: '/passrecords' + toQueryString(parameter),
    method: 'get',
    data: parameter
  })
}
