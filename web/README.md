## 门岗守卫Web管理端    

## 开发    

### 安装依赖    
```shell
npm install
```

### 配置后端服务地址和代理    
打开`vue.config.js`文件，找到`devServer`节点修改即可    

### 打包     
```shell
npm run build
```
也可使用在`release`文件夹中打好的包直接部署。

## 部署    
### `http-server`部署（仅限非生产环境）

#### 安装`http-server`
```shell
npm i http-server -g
npm i http-server-with-proxy-path -g
```
进入`dist`目录，执行:    
```shell
http-server -P  http://localhost:8080  -R /api/
```

访问`http://localhost:[port]/index.html`即可。

### Nginx部署

将`/api`路径反向代理到后端服务即可。

```shell
    location /api {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass [服务器地址和端口];
    }
```
