// 管理账号信息
const USERS_KEY = 'USERS_KEY';
const STATE_KEY = 'STATE_KEY';
let TOKEN = ''
let SERVER = ''

const getUsers = function() {
	let ret = '';
	ret = uni.getStorageSync(USERS_KEY);
	if (!ret) {
		ret = '[]';
	}
	return JSON.parse(ret);
}

const addUser = function(userInfo) {
	let users = getUsers();
	users.push({
		account: userInfo.account,
		password: userInfo.password
	});
	uni.setStorageSync(USERS_KEY, JSON.stringify(users));
}

const setToken = function(newToken) {
  TOKEN = 'Bearer ' + newToken
}

const getToken = function() {
  return TOKEN
}

const setServer = function(newServer) {
  SERVER = newServer
}

const getServer = function() {
  return SERVER
}

export default {
	getUsers,
	addUser,
  getToken,
  setToken,
  setServer,
  getServer
}
